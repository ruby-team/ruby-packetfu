ruby-packetfu (2.0.0-2) unstable; urgency=medium

  * Team upload.
  * d/p/0003-Skip-tests-accessing-the-Internet.patch: fix FTBFS
    (Closes: #1080465).

 -- Lucas Kanashiro <kanashiro@debian.org>  Mon, 09 Sep 2024 12:26:36 -0300

ruby-packetfu (2.0.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Remove patches, not needed anymore.
  * d/p/0001-Do-not-depend-on-coveralls.patch: fix tests during build time.
  * d/p/0002-Skip-tests-requiring-network-access.patch: some tests require
    root privilege.
  * Declare compliance with Debian Policy 4.7.0.

 -- Lucas Kanashiro <kanashiro@debian.org>  Wed, 04 Sep 2024 18:59:32 -0300

ruby-packetfu (1.1.11-3) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * Use https:// in Vcs-* fields
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Use secure URI in debian/watch.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Update watch file format version to 4.
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on ruby-rspec.
    + ruby-packetfu: Drop versioned constraint on ruby-pcaprub in Depends.

  [ Lucas Kanashiro ]
  * Refresh patch
  * d/p/0004-Fix-FTBFS-with-Ruby-3.2.patch: do not use method and const
    removed from ruby 3.2
  * Bump debhelper compat level to 13
  * Declare compliance with Debian Policy 4.6.2
  * d/control: depend on ${ruby:Depends} instead of the ruby interpreter

 -- Lucas Kanashiro <kanashiro@debian.org>  Thu, 22 Feb 2024 21:18:51 -0300

ruby-packetfu (1.1.11-2) unstable; urgency=medium

  * Team upload.
  * Create patch fixing tests with ruby2.3 (Closes: #816254)
  * d/copyright: add Upstream-Contact and set up upstream email
  * Update Debian packaging copyright
  * Declare compliance with Debian policy 3.9.7

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Mon, 29 Feb 2016 16:36:03 -0300

ruby-packetfu (1.1.11-1) unstable; urgency=low

  * New upstream version.
  * Ship some upstream documentation.
  * Update debian/copyright.
  * Tighten pcaprub dependency.
  * Refresh and drop obsolete patches.
  * Add a patch to use Ruby 2.1+ new API instead of the unpackaged
    network_interface library.
  * Update Vcs-Git URL.
  * Switch to debhelper compat level 9.
  * Override lintian regarding the OUI database. It's only present in the
    source and shipped in the binary package.

 -- Jérémy Bobbio <lunar@debian.org>  Thu, 10 Sep 2015 16:46:27 +0200

ruby-packetfu (1.1.10-2) unstable; urgency=medium

  * Team upload
  * Update packaging with dh-make-ruby -w
    + Bump Standards-Version to 3.9.6 (no changes needed)
  * Build-depend on ruby-rspec >=3 and ruby-rspec-its (Closes: #795044)
  * Drop dependency on ruby-rspec (not needed)
  * Patches:
    + refresh 0001-Be-better-about-version-checking-multidigits.patch
    + refresh 0002-Do-not-require-rubygems-even-with-Ruby-1.8.patch
    + add 0004-Port-to-rspec3.patch (upstream) adding support for rspec3
    + add 0005-Require-rspec-its.patch to explicitly load rspec/its
  * Restrict examples to Ruby files and symlink oui.txt from ieee-data package

 -- Cédric Boutillier <boutil@debian.org>  Mon, 10 Aug 2015 01:27:34 +0200

ruby-packetfu (1.1.10-1) unstable; urgency=low

  * New upstream version.
  * Drop an obsolete patch.
  * Import upstream patch to fix test suite compatibility with version
    numbers and Ruby 2.x.
  * Re-enable support for all versions. (Closes: #725675, #746098)
  * Update homepage location.
  * Bump Standards-Version, no changes required.

 -- Jérémy Bobbio <lunar@debian.org>  Sat, 03 May 2014 15:12:08 +0200

ruby-packetfu (1.1.8-2) unstable; urgency=low

  * Only provide ruby1.9.1 compatibility until issues with ruby2.0 are sorted
    out. (Closes: #725556)

 -- Jérémy Bobbio <lunar@debian.org>  Mon, 07 Oct 2013 11:20:01 +0200

ruby-packetfu (1.1.8-1) unstable; urgency=low

  * Initial release. (Closes: #719297)

 -- Jérémy Bobbio <lunar@debian.org>  Sat, 24 Aug 2013 19:25:47 +0200
